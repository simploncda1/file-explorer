package co.simplon.alt6;

import java.io.File;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import co.simplon.alt6.exception.AlreadyAtTheRootException;

public class Navigation {

    private Path currentPath;
    List<Favorite> favoriteDirectories;

    /**
     * Constructeur de la classe Navigation.
     * Initialise le chemin actuel au répertoire de travail de l'utilisateur.
     */
    public Navigation() {
        currentPath = Paths.get("").toAbsolutePath();
        favoriteDirectories = new ArrayList<>();
    }

    /**
     * Initialise une nouvelle instance de Navigation avec une liste de répertoires
     * favoris.
     *
     * @param initialFavorites La liste de répertoires favoris à utiliser pour
     *                         l'initialisation.
     */
    public Navigation(List<Favorite> initialFavorites) {
        this.favoriteDirectories = new ArrayList<>(initialFavorites);
    }

    /**
     * Met à jour le chemin actuel (emplacement actuel) avec le nouveau chemin
     * spécifié.
     *
     * @param newPath Le nouveau chemin à définir comme emplacement actuel.
     */
    public void setCurrentPath(Path newPath) {
        currentPath = newPath;
    }

    /**
     * Récupère le chemin actuel (emplacement actuel).
     *
     * @return Le chemin actuel sous forme de chaîne de caractères.
     */
    public String currentLocation() {
        return currentPath.toString();
    }

    /**
     * Déplace le chemin actuel vers un nouvel emplacement spécifié.
     * 
     * @param location Le chemin relatif ou absolu de la nouvelle destination.
     * @return Le chemin absolu mis à jour du répertoire actuel.
     * @throws IOException Si l'emplacement spécifié n'existe pas.
     */
    public String goTo(String location) throws IOException {
        Path newPath = currentPath.resolve(location).toAbsolutePath();
        if (newPath.toFile().exists()) {
            currentPath = newPath;
            return currentPath.toString();
        } else {
            throw new NoSuchFileException(location);
        }
    }

    /**
     * Déplace le chemin actuel vers le répertoire parent.
     * 
     * @return Le chemin absolu mis à jour du répertoire actuel.
     * @throws AlreadyAtTheRootException Si le chemin actuel est déjà à la racine de
     *                                   l'arborescence.
     */
    public String goBackward() throws AlreadyAtTheRootException {
        if (currentPath.getParent() != null) {
            currentPath = currentPath.getParent();
            return currentPath.toString();
        } else {
            throw new AlreadyAtTheRootException();
        }
    }

    /**
     * Liste les fichiers et répertoires dans le répertoire actuel.
     * 
     * @return Un tableau d'objets File représentant les fichiers et répertoires.
     */
    public File[] showFile() {
        File currentDirectory = new File(currentPath.toString());
        return currentDirectory.listFiles();
    }

    /**
     * Recherche récursivement les fichiers et répertoires contenant le nom de
     * fichier recherché à partir du répertoire spécifié.
     *
     * @param fileName          Le nom de fichier recherché.
     * @param startingDirectory Le répertoire de départ pour la recherche.
     * @return Une liste des chemins complets des fichiers et répertoires
     *         correspondants à la recherche.
     */
    public List<String> finding(String fileName, String startingDirectory) {
        File startingDir = new File(startingDirectory);

        if (!startingDir.exists() || !startingDir.isDirectory()) {
            return new ArrayList<>();
        }

        List<String> matchingPaths = new ArrayList<>();
        File[] files = startingDir.listFiles();

        if (files != null) {
            for (File file : files) {
                if (file.getName().contains(fileName)) {
                    matchingPaths.add(file.getAbsolutePath());
                }
                if (file.isDirectory()) {
                    matchingPaths.addAll(finding(fileName, file.getAbsolutePath()));
                }
            }
        }

        return matchingPaths;
    }

    /**
     * Ajoute un répertoire favori à la liste des répertoires favoris.
     *
     * @param url  L'URL du répertoire favori.
     * @param name Le nom du répertoire favori.
     */
    public void addFavorite(String url, String name) {
        Favorite favorite = new Favorite(url, name);
        favoriteDirectories.add(favorite);
    }

    /**
     * Récupère la liste des répertoires favoris.
     *
     * @return Une liste des répertoires favoris sous forme d'objets Favorite.
     */
    public List<Favorite> getFavoriteDirectories() {
        return favoriteDirectories;
    }

}
