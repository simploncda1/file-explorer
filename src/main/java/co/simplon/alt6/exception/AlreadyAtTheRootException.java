package co.simplon.alt6.exception;

public class AlreadyAtTheRootException extends Exception {
    
    public AlreadyAtTheRootException()
    {
        super("Vous êtes déjà à la racine de l'arborescence.");
    }
}
