package co.simplon.alt6;

import java.util.Objects;

public class Favorite {
    private String url;
    private String name;

    public Favorite(String url, String name) {
        this.url = url;
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    /**
     * Compare cet objet Favorite à un autre objet pour déterminer s'ils sont égaux.
     *
     * @param o L'objet à comparer à cet objet Favorite.
     * @return true si les objets sont égaux en fonction de leurs attributs url et
     *         name, sinon false.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Favorite favorite = (Favorite) o;
        return Objects.equals(url, favorite.url) &&
                Objects.equals(name, favorite.name);
    }

    /**
     * Calcule un code de hachage pour cet objet Favorite.
     *
     * @return Le code de hachage calculé en fonction des attributs url et name de
     *         l'objet.
     */
    @Override
    public int hashCode() {
        return Objects.hash(url, name);
    }
}
