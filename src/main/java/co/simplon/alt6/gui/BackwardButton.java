package co.simplon.alt6.gui;

import co.simplon.alt6.exception.AlreadyAtTheRootException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;

public class BackwardButton extends Button implements EventHandler<ActionEvent> {

    ListView<String> folderListView;
    FileExplorerGui gui;

    public BackwardButton(ListView<String> folderListView, FileExplorerGui gui) {
        super("<--");
        this.folderListView = folderListView;
        this.gui = gui;
        setStyle("-fx-padding: 10");
        setOnAction(this);
    }

    @Override
    public void handle(ActionEvent event) {
        try {
            gui.getNavigation().goBackward();
            gui.updateFolderListView(folderListView);
            gui.getPathTextField().setPromptText(gui.getNavigation().currentLocation());
        } catch (AlreadyAtTheRootException e) {
            gui.getPathTextField().setPromptText("Vous êtes déjà à la racine de l'arborescence !");
            e.printStackTrace();
        }
    }
}