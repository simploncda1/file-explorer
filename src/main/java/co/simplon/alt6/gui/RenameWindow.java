package co.simplon.alt6.gui;

import co.simplon.alt6.Files;
import co.simplon.alt6.Navigation;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class RenameWindow extends Application{
    
    private TextField renameFileOrDirectory = new TextField();
    private Navigation navigation;

    
    public void start(Stage stage, ListView<String> folderListView, FileExplorerGui aFileExplorerGui) throws Exception {
        GridPane grid = new GridPane();
        navigation = new Navigation();
        Button renameButton = new Button("Renommer");
        renameButton.setOnAction(e -> renameFileOrDirectory(stage,folderListView, aFileExplorerGui));
        grid.add(renameFileOrDirectory,0,0,2,1);
        grid.add(renameButton,1,1);
        
        Scene scene = new Scene(grid);
        stage.setScene(scene);
        stage.setTitle("Renommer");
        stage.show();

        
    }

    private void renameFileOrDirectory(Stage stage, ListView<String> folderListView, FileExplorerGui aFileExplorerGui){
        
        String newPath = navigation.currentLocation() + "/";
        String newFileName = navigation.showFile()[folderListView.getSelectionModel().getSelectedIndex()]
        .getName();
        Files theFilesToRename = new Files(newFileName, newPath);
        // System.out.println(theFilesToRename.getFileName());
        if(theFilesToRename.getMyFile().isFile()){
            theFilesToRename.renameFile(renameFileOrDirectory.getText());
            // System.out.println("it is file");
        }
        else{
            theFilesToRename.renameDirectory(renameFileOrDirectory.getText());
            // System.out.println("bb");
        }
        aFileExplorerGui.updateFolderListView(folderListView);
        stage.close();
    }

    @Override
    public void start(Stage arg0) throws Exception {
        
    }
}
