package co.simplon.alt6.gui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;

import co.simplon.alt6.Favorite;
import co.simplon.alt6.Files;
import co.simplon.alt6.Navigation;
import co.simplon.alt6.exception.AlreadyAtTheRootException;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

public final class FileExplorerGui extends Application {
    private TextField searchTextField; // Barre de recherche
    private TextField pathTextField; // Barre d'URL de fichier
    private Files theFilesToDelete;
    private Navigation navigation;
    private CreateWindow cw;
    private RenameWindow rw;

    public Navigation getNavigation() {
        return navigation;
    }

    public TextField getPathTextField() {
        return pathTextField;
    }

    @Override
    public void start(Stage stage) {
        //Initialise les fenêtres et un nouveau stage
        cw = new CreateWindow();
        rw = new RenameWindow();
        Stage newStage = new Stage();
        //Initialise navigation
        navigation = new Navigation();

        List<Button> listButton = new ArrayList<>();
        BorderPane root = new BorderPane();

        File[] filesAndDirectories = navigation.showFile();
        // Barre d'URL de fichier et Barre de recherche en haut
        HBox topBar = new HBox();
        VBox rightVerticalBar = new VBox();

        // Barre d'URL
        pathTextField = new TextField();

        pathTextField.setPromptText(navigation.currentLocation());
        HBox.setHgrow(pathTextField, Priority.ALWAYS);
        pathTextField.setStyle("-fx-padding: 10;");

        // Barre de recherche
        searchTextField = new TextField();
        searchTextField.setPromptText("Rechercher...");
        HBox.setHgrow(searchTextField, Priority.SOMETIMES);
        searchTextField.setStyle("-fx-padding: 10;");

        // Liste des dossiers à droite
        ListView<String> folderListView = new ListView<>();
        updateFolderListView(folderListView);

        // Bouton retour en arrière <-
        BackwardButton backwardButton = new BackwardButton(folderListView, this);

        // Favoris à gauche
        showFavorite();

        // Liste des noms de favoris
        List<Favorite> favoriteList = navigation.getFavoriteDirectories();
        List<String> favoriteNames = new ArrayList<>();
        for (Favorite favorite : favoriteList) {
            favoriteNames.add(favorite.getName());
        }
        ListView<String> favoritesListView = new ListView<>();
        favoritesListView.getItems().addAll(favoriteNames);

        searchTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            List<String> searchResults = navigation.finding(newValue, navigation.currentLocation());
            folderListView.getItems().setAll(searchResults);
            handleClickSearchView(folderListView, searchResults);
        });

        // Gestion de la navigation des Favoris
        goToFavorite(favoriteList, favoritesListView, folderListView);

        root.setLeft(favoritesListView);

        topBar.getChildren().addAll(backwardButton, pathTextField, searchTextField);
        root.setTop(topBar);

        root.setCenter(folderListView);

        Button deleteButton = new Button("Supprimer");
        Button createButton = new Button("Nouveau ...");
        Button renameButton = new Button("Renommer");
        listButton.add(deleteButton);
        listButton.add(renameButton);
        // deleteButton.setDisable(true);
        // renameButton.setDisable(true);
        deleteButton.setMinWidth(80);
        createButton.setMinWidth(80);
        renameButton.setMinWidth(80);
        deleteButton.setMinHeight(60);
        createButton.setMinHeight(60);
        renameButton.setMinHeight(60);
        rightVerticalBar.getChildren().addAll(deleteButton, createButton, renameButton);
        deleteButton.setOnAction(e -> delete(folderListView));

        root.setRight(rightVerticalBar);

        Scene scene = new Scene(root, 800, 600);
        stage.setScene(scene);
        stage.setTitle("Explorateur de fichiers");
        stage.show();
        // folderListView.setOnMouseClicked(e -> notDisableButton(listButton, e));

        
        createButton.setOnAction(e -> openCreateFileWindow(folderListView, cw, newStage));
        renameButton.setOnAction(e -> openRenameWindow( folderListView, rw, newStage));
        folderListView.setOnMouseClicked(event -> setUpMouseClick(event, folderListView, filesAndDirectories, deleteButton));
        updateFolderListView(folderListView);
        
    }

    
     /**
     * WIP
     */
    private void setUpMouseClick(MouseEvent event, ListView<String> folderListView, File[] filesAndDirectories, Button deleteButton) {
        
        //System.out.println(event.getClickCount());
        
                
        if (event.getClickCount() == 2) {
                int selectedIndex = folderListView.getSelectionModel().getSelectedIndex();
                if (selectedIndex >= 0 && selectedIndex < folderListView.getItems().size()) {
                    String selectedPath = folderListView.getItems().get(selectedIndex);
                    try {
                        navigation.goTo(selectedPath);
                        updateFolderListView(folderListView);
                        pathTextField.setPromptText(navigation.currentLocation());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            
        }
        return;
    }


    /**
     * Supprime un fichier
     * 
     * @param folderListView
     */
    private void delete( ListView<String> folderListView){
        String newPath = navigation.currentLocation() + "/";
        String newFileName = navigation.showFile()[folderListView.getSelectionModel().getSelectedIndex()]
        .getName();
        theFilesToDelete = new Files(newFileName, newPath);
        //System.out.println(theFilesToDelete.getFileName());
        theFilesToDelete.deleteFileOrDirectory();
        updateFolderListView(folderListView);
    }

    
     /**
     * 
     * Ouvre une fenêtre pour renommer un fichier
     * 
     * @param folderListView
     * @param rw
     * @param stageForNewWindow
     */
    private void openRenameWindow(ListView<String> folderListView, RenameWindow rw, Stage stageForNewWindow) {
        try {
            rw.start(stageForNewWindow, folderListView, this);
        } catch (Exception e1) {
            
            e1.printStackTrace();
        }

    }


    /**
     * Ouvre la fenêtre pour crée un fichier / dossier
     * 
     * @param folderListView
     * @param cw
     * @param stageForNewWindow
     */
    private void openCreateFileWindow(ListView<String> folderListView, CreateWindow cw, Stage stageForNewWindow) {
        
        try {
            cw.start(stageForNewWindow, this, folderListView);

        } catch (Exception e1) {
            
            e1.printStackTrace();
        }
    }


    /**
     * WIP
     * Active les boutons si item dans folderListView selectionné
     * 
     * @param listButton
     * @param folderListView
     */
    private void notDisableButton(List<Button> listButton, ListView<String> folderListView) {
        folderListView.setOnMouseClicked(e -> {
        if(e.getButton() == MouseButton.PRIMARY){
                for (Button btn : listButton) {
                    btn.setDisable(false);
                }
            } 

        });
    }

    /**
     * Gère la sélection d'un favori dans la liste des favoris.
     * Lorsqu'un favori est sélectionné, cette méthode navigue vers l'URL associée
     * au favori
     * et met à jour la vue des dossiers.
     *
     * @param favoriteList      La liste des favoris.
     * @param favoritesListView La vue ListView des favoris.
     * @param folderListView    La vue ListView des dossiers.
     */
    private void goToFavorite(
            List<Favorite> favoriteList,
            ListView<String> favoritesListView,
            ListView<String> folderListView) {
        favoritesListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                String selectedFavoriteName = newValue;
                for (Favorite favorite : favoriteList) {
                    if (favorite.getName().equals(selectedFavoriteName)) {
                        String favoriteUrl = favorite.getUrl();
                        try {
                            navigation.goTo(favoriteUrl);
                        } catch (IOException e) {
                            e.printStackTrace();
                            System.out.println("Mauvais chemin renseigné");
                        }
                        updateFolderListView(folderListView);
                        pathTextField.setPromptText(navigation.currentLocation());
                        break;
                    }
                }
            }
        });
    }

    /**
     * Affiche les favoris en fonction du système d'exploitation.
     * Ajoute les favoris préconfigurés en fonction du système d'exploitation.
     */
    private void showFavorite() {
        String osName = System.getProperty("os.name").toLowerCase();
        if (osName.contains("windows")) {
            navigation.addFavorite("C:\\Users\\Utilisateur\\OneDrive\\Bureau", "Bureau");
            navigation.addFavorite("C:\\Users\\Utilisateur\\Downloads", "Téléchargements");
            navigation.addFavorite("C:\\Users\\Utilisateur\\OneDrive\\Documents", "Documents");
            navigation.addFavorite("C:\\Users\\Utilisateur\\OneDrive\\Images", "Images");
            navigation.addFavorite(navigation.currentLocation(), "Default");
        } else if (osName.contains("linux") || osName.contains("unix")) {
            navigation.addFavorite("/home/alexandre/Bureau", "Bureau");
            navigation.addFavorite("/home/alexandre/Images", "Images");
            navigation.addFavorite("/home/alexandre/Téléchargements", "Téléchargements");
            navigation.addFavorite(navigation.currentLocation(), "Default");
        } else {
            System.out.println("Pas de bol, Jean-Paul !");
        }
    }


    /**
     * Navigue vers le répertoire parent et rafraîchit la vue des dossiers.
     *
     * @param folderListView La vue ListView des dossiers.
     * @param backwardButton Le bouton de retour en arrière.
     */
    private void navigateBackAndRefresh(ListView<String> folderListView, Button backwardButton) {
        try {
            navigation.goBackward();
            updateFolderListView(folderListView);
            pathTextField.setPromptText(navigation.currentLocation());
        } catch (AlreadyAtTheRootException e) {
            backwardButton.setDisable(true);
            e.printStackTrace();
        }
    }

   

   

    /**
     * Met à jour le contenu du ListView avec la liste des dossiers et fichiers
     * présents dans le répertoire actuel.
     *
     * @param folderListView Le ListView à mettre à jour.
     */
    protected void updateFolderListView(ListView<String> folderListView) {
        folderListView.getItems().clear();
        File[] filesAndDirectories = navigation.showFile();
        if (filesAndDirectories != null) {
            for (File fileOrDirectory : filesAndDirectories) {
                if (fileOrDirectory.isDirectory()) {
                    String folderName = fileOrDirectory.getName();
                    Files files = new Files(folderName);
                    folderListView.getItems().add("Dossier : " + files.getFileName());

                    folderListView.setOnMouseClicked(event -> {
                        if (event.getClickCount() == 2) {
                            String newPath = navigation.currentLocation() + "/"
                                    + filesAndDirectories[folderListView.getSelectionModel().getSelectedIndex()]
                                            .getName();
                            try {
                                navigation.goTo(newPath);
                                updateFolderListView(folderListView);
                                pathTextField.setPromptText(navigation.currentLocation());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } else {
                    Files files = new Files(fileOrDirectory.getName());
                    folderListView.getItems().add("Fichier : " + files.getFileName());
                }
            }
        }
    }


    /**
     * Gère le double-clic dans la vue de recherche.
     *
     * @param folderListView La vue ListView des dossiers.
     * @param searchResults  La liste des résultats de recherche.
     */
    private void handleClickSearchView(ListView<String> folderListView, List<String> searchResults) {
        folderListView.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2) {
                int selectedIndex = folderListView.getSelectionModel().getSelectedIndex();
                if (selectedIndex >= 0 && selectedIndex < searchResults.size()) {
                    String selectedPath = searchResults.get(selectedIndex);
                    File file = new File(selectedPath);
                    if (file.isDirectory()) {
                        try {
                            navigation.goTo(selectedPath);
                            updateFolderListView(folderListView);
                            pathTextField.setPromptText(navigation.currentLocation());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (file.isFile()) {
                        try {
                            String osName = System.getProperty("os.name").toLowerCase();
                            System.out.println(selectedPath);
                            if (osName.contains("win")) {
                                Runtime.getRuntime().exec("cmd /c start " + selectedPath);
                            } else if (osName.contains("nix") || osName.contains("nux") ||
                                    osName.contains("mac")) {
                                Runtime.getRuntime().exec("xdg-open " + selectedPath);
                            } else {
                                System.out.println("Système d exploitation non pris en charge.");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    /**
     * Gère le double-clic dans la vue des dossiers.
     *
     * @param folderListView      La vue ListView des dossiers.
     * @param filesAndDirectories Le tableau des fichiers et dossiers dans le
     *                            répertoire actuel.
     */
    private void handleDoubleClick(ListView<String> folderListView, File[] filesAndDirectories) {
        folderListView.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2) {
                String newPath = navigation.currentLocation() + "/"
                        + filesAndDirectories[folderListView.getSelectionModel().getSelectedIndex()]
                                .getName();
                File file = new File(newPath);
                if (file.isDirectory()) {
                    try {
                        navigation.goTo(newPath);
                        updateFolderListView(folderListView);
                        pathTextField.setPromptText(navigation.currentLocation());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (file.isFile()) {
                    try {
                        String osName = System.getProperty("os.name").toLowerCase();
                        if (osName.contains("win")) {
                            Runtime.getRuntime().exec("cmd /c start " + newPath);
                        } else if (osName.contains("nix") || osName.contains("nux") ||
                                osName.contains("mac")) {
                            Runtime.getRuntime().exec("xdg-open " + newPath);
                        } else {
                            System.out.println("Système d exploitation non pris en charge.");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }



    public static void main(String[] args) {
        launch(args);
    }

}
