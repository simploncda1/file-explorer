package co.simplon.alt6.gui;

import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import co.simplon.alt6.Files;
import co.simplon.alt6.Navigation;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class CreateWindow extends Application {

    private TextField createFileOrDirectory = new TextField();

    public void start(Stage stage, FileExplorerGui aFileExplorerGui, ListView<String> folderListView) throws Exception {
        GridPane grid = new GridPane();

        Button createFileButton = new Button("Crée un fichier");
        Button createDirectoryButton = new Button("Crée un dossier");
        grid.add(createFileOrDirectory, 0, 0, 2, 1);
        grid.add(createFileButton, 0, 1);
        grid.add(createDirectoryButton, 1, 1);
        Scene scene = new Scene(grid);
        stage.setScene(scene);
        stage.setTitle("Nouveau ...");
        stage.show();

        createFileButton.setOnAction(e -> createNewFile(stage, aFileExplorerGui, folderListView));
        createDirectoryButton.setOnAction(e -> createNewDirectory(stage, aFileExplorerGui, folderListView));
    }

    private void createNewFile(Stage stage, FileExplorerGui aFileExplorerGui, ListView<String> folderListView) {
        Files newFile = new Files(createFileOrDirectory.getText(), aFileExplorerGui.getNavigation().currentLocation());
        Navigation n = new Navigation();
        n.currentLocation();
        System.out.println(n.currentLocation());
        newFile.createFile();

        aFileExplorerGui.updateFolderListView(folderListView);
        stage.close();

    }

    private void createNewDirectory(Stage stage, FileExplorerGui aFileExplorerGui, ListView<String> folderListView) {
        Files newFile = new Files(createFileOrDirectory.getText(), aFileExplorerGui.getNavigation().currentLocation());
        newFile.createDirectory();
        aFileExplorerGui.updateFolderListView(folderListView);
        stage.close();

    }

    @Override
    public void start(Stage arg0) throws Exception {

    }

}
