package co.simplon.alt6;

import java.io.File;
import java.io.IOException;

public class Files {
    private Navigation nav = new Navigation();
    private File myFile;
    private String fileName;
    private String currentPath = nav.currentLocation();

    public Files(String fileName, String currentPath) {
        this.fileName = fileName;
        this.currentPath = currentPath;
        myFile = new File(currentPath + File.separator + fileName);
    }

    public Files() {
        fileName = "New_File";
        myFile = new File(currentPath + File.separator + fileName);

    }

    public Files(String aFileName) {
        fileName = aFileName;
        myFile = new File(currentPath + File.separator + fileName);
    }

    public void createFile() {
        myFile = new File(myFile.getAbsolutePath() + ".txt");

        // Peut être le fait de pouvoir créer n'importe quel type de fichier
        // myFile = new File(myFile.getAbsolutePath());
        
        if (!myFile.exists()) {
            try {
                myFile.createNewFile();
            } catch (IOException e) {
                System.out.println("Le fichier n'a pas pu être créé.");
            }
        } else {
            System.out.println("Le fichier existe déjà.");
        }
    }

    private void createFile(String aFileName) {
        this.fileName = aFileName;
        myFile = new File(currentPath + File.separator + this.fileName);
        this.createFile();
    }

    public void createDirectory() {
        boolean isCreated;
        isCreated = myFile.mkdir();
        if (isCreated) {
            myFile.mkdir();
        } else {
            System.out.println("Le dossier existe déja.");
        }
    }

    private void createDirectory(String aDirectoryName) {
        this.fileName = aDirectoryName;
        myFile = new File(currentPath + File.separator + this.fileName);
        this.createDirectory();

    }

    public void deleteFileOrDirectory() {
        boolean isDeleted;
        //System.out.println(myFile.getAbsolutePath());
        isDeleted = myFile.delete();
        if (isDeleted) {
            myFile.delete();
        } else {
            System.out.println("Le dossier ou le fichier n'a pas pu être supprimé ou il n'existe déjà plus.");
        }
    }

    public void renameFile(String newFileName) {
        this.deleteFileOrDirectory();
        this.setFileName(newFileName);
        this.createFile(newFileName);
    }

    public void renameDirectory(String newDirectoryName) {
        this.deleteFileOrDirectory();
        this.setFileName(newDirectoryName);
        this.createDirectory(newDirectoryName);
    }

    public void moveFile(String newPath) {
        this.deleteFileOrDirectory();
        currentPath = newPath;
        this.createFile(this.fileName);

    }

    public void moveDirectory(String newPath) {
        this.deleteFileOrDirectory();
        currentPath = newPath;
        this.createDirectory(this.fileName);

    }

    public File getMyFile() {
        return myFile;
    }

    public String getFileName() {
        return fileName;
    }

    public String getCurrentPath() {
        return currentPath;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
        myFile = new File(currentPath + File.separator + fileName);
    }

     
}
