package co.simplon.alt6;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class filesTest {
    private Files fileTest;
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    void setUp(){
        
        System.setOut(new PrintStream(outputStreamCaptor));
        fileTest = new Files();
        if(fileTest.getMyFile().exists()){
            fileTest.deleteFileOrDirectory();
        }
        
    }

    @Test
    void testCreatedFile(){
        fileTest.createFile();
        boolean result = fileTest.getMyFile().exists();
        assertTrue(result);
    }

    @Test
    void testCanNotCreatedFileIfExists() {
        String name = fileTest.getFileName();
        fileTest.createFile();
        fileTest.setFileName(name);
        fileTest.createFile();
        assertEquals("Le fichier existe déjà.", outputStreamCaptor.toString().trim());
    }

    @Test
    void testCanNotCreatedFileWithWrongCharacter() {
        fileTest.setFileName("test_/v2");
        fileTest.createFile();
        assertEquals("Le fichier n'a pas pu être créé.", outputStreamCaptor.toString().trim());
    }

   

    @Test
    void testCreateDirectory(){
        fileTest.createDirectory();
        boolean isExists = fileTest.getMyFile().exists();
        assertTrue(isExists);
        
    }

     @Test
    void testCanNotCreatedDirectory(){
        fileTest.createDirectory();
        Files otherFiles = new Files();
        otherFiles.createDirectory();
        assertEquals("Le dossier existe déja.", outputStreamCaptor.toString()
       .trim());

    }


    @Test
    void testDeleteFileOrDirectory(){
        fileTest.createFile();
        fileTest.deleteFileOrDirectory();
        boolean isExists = fileTest.getMyFile().exists();
        assertFalse(isExists);
    }

    @Test
    void testCanNotDeleteFileOrDirectory(){

        fileTest.deleteFileOrDirectory();
        assertEquals("Le dossier ou le fichier n'a pas pu être supprimé ou il n'existe déjà plus.", outputStreamCaptor.toString()
       .trim());
    }

    @Test
    void testRenameFile(){
        
        fileTest.createFile();
        fileTest.renameFile("test");
        assertEquals("test", fileTest.getFileName());
    }



    @Test
    void testRenameDirectory(){
        fileTest.createDirectory();
        fileTest.renameDirectory("New_Directory");
        assertEquals("New_Directory", fileTest.getFileName());
    }

    @Test
    void testMoveFile(){
        fileTest.createFile();
        String test = fileTest.getCurrentPath();
        fileTest.moveFile(fileTest.getCurrentPath() + "\\src\\main\\resources");
        assertNotEquals(test, fileTest.getCurrentPath());
    }

    @Test
    void testMoveDirectory(){
        fileTest.createDirectory();
        String test = fileTest.getCurrentPath();
        fileTest.moveDirectory(fileTest.getCurrentPath() + "\\src\\main\\resources");
        assertNotEquals(test, fileTest.getCurrentPath());
    }

    @AfterEach
    void tearDown() {
        System.setOut(standardOut);
        if(fileTest.getMyFile().exists()){
            fileTest.deleteFileOrDirectory();
        }
    }
}
