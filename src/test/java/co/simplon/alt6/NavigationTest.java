package co.simplon.alt6;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import co.simplon.alt6.exception.AlreadyAtTheRootException;

public class NavigationTest {

    private Navigation navigation;

    /**
     * Méthode de configuration exécutée avant chaque test unitaire.
     * Elle crée une nouvelle instance de la classe Navigation pour garantir
     * que chaque test part d'un environnement propre et indépendant.
     */
    @BeforeEach
    void setUp() {
        navigation = new Navigation();
    }

    /**
     * Teste si l'objet Navigation est correctement instancié.
     * Vérifie que l'objet n'est pas nul.
     */
    @Test
    void testShouldInstanciate() {
        assertNotNull(navigation);
    }

    /**
     * Teste la méthode currentLocation() pour obtenir le chemin actuel.
     * Vérifie si le chemin actuel retourné correspond au répertoire de travail de
     * l'utilisateur.
     */
    @Test
    void testCurrentLocation() {
        assertEquals(System.getProperty("user.dir"), navigation.currentLocation());
    }

    /**
     * Teste la méthode goTo() pour déplacer le chemin actuel vers une destination
     * spécifique.
     * Vérifie si le chemin actuel après l'appel de goTo() correspond à
     * l'emplacement attendu.
     * 
     * @throws IOException Si l'emplacement spécifié n'existe pas.
     */
    @Test
    void testGoToSpecificLocation() throws IOException {
        String initialLocation = System.getProperty("user.dir");
        String result = navigation.goTo("src/main/java/co/simplon/alt6/Main.java");
        String expectedLocation = initialLocation + "/src/main/java/co/simplon/alt6/Main.java";
        assertEquals(expectedLocation, result);
    }

    /**
     * Teste la méthode goTo() lorsque l'emplacement spécifié n'existe pas.
     * Vérifie si une exception NoSuchFileException est lancée.
     */
    @Test
    void testGoTo404() {
        assertThrows(NoSuchFileException.class, () -> {
            navigation.goTo("src/main/java/co/simplon/alt6/FidoLeCorgi");
        });
    }

    /**
     * Teste la méthode goBackward() pour revenir au répertoire parent.
     * Vérifie si le chemin actuel après l'appel de goBackward() correspond à
     * l'emplacement attendu.
     * 
     * @throws IOException               Si une erreur d'entrée-sortie se produit.
     * @throws AlreadyAtTheRootException Si le chemin actuel est déjà à la racine de
     *                                   l'arborescence.
     */
    @Test
    void testGoBackward() throws IOException, AlreadyAtTheRootException {
        String initialLocation = System.getProperty("user.dir");
        navigation.goTo("src/main/java/co/simplon/alt6/Main.java");
        String result = navigation.goBackward();
        String expectedLocation = initialLocation + "/src/main/java/co/simplon/alt6";
        assertEquals(expectedLocation, result);
    }

    /**
     * Teste la méthode goBackward() lorsque le chemin actuel est déjà à la racine.
     * Vérifie si une exception AlreadyAtTheRootException est lancée.
     * 
     * @throws IOException               Si une erreur d'entrée-sortie se produit.
     * @throws AlreadyAtTheRootException Si le chemin actuel est déjà à la racine de
     *                                   l'arborescence.
     */
    @Test
    void testGoBackwardFail() throws IOException, AlreadyAtTheRootException {
        navigation.goTo("/");
        assertThrows(AlreadyAtTheRootException.class, () -> {
            navigation.goBackward();
        });
    }

    /**
     * Teste si la méthode showFile retourne la liste des fichiers dans le
     * répertoire courant.
     */
    @Test
    void testShowFile() {
        File[] files = navigation.showFile();
        StringBuilder resultBuilder = new StringBuilder();
        for (File file : files) {
            resultBuilder.append(file.getName()).append("\n");
        }
        String result = resultBuilder.toString();
        String expected = ".gitignore\n.git\nREADME.md\n.vscode\nsrc\ntarget\npom.xml\n";
        assertEquals(expected, result);
    }

    /**
     * Teste si la méthode finding recherche avec succès un fichier dans le même
     * répertoire que le répertoire de travail de l'utilisateur.
     */
    @Test
    void testFindingInSameLocation() {
        String initialLocation = System.getProperty("user.dir");
        String fileNameToSearch = "po";
        List<String> matchingFiles = navigation.finding(fileNameToSearch, initialLocation);
        boolean fileFound = false;
        for (String filePath : matchingFiles) {
            if (filePath.endsWith("pom.xml")) {
                fileFound = true;
                break;
            }
        }
        assertTrue(fileFound);
    }

    /**
     * Teste si la méthode finding recherche avec succès un fichier en profondeur
     * dans les répertoires à partir du répertoire de travail de l'utilisateur.
     */
    @Test
    void testFindingInDepth() {
        String initialLocation = System.getProperty("user.dir");
        String fileNameToSearch = "oot";
        List<String> matchingFiles = navigation.finding(fileNameToSearch, initialLocation);
        boolean fileFound = false;
        for (String filePath : matchingFiles) {
            if (filePath.endsWith("AlreadyAtTheRootException.java")) {
                fileFound = true;
                break;
            }
        }
        assertTrue(fileFound);
    }

    /**
     * Teste si la méthode finding gère correctement le cas où le fichier recherché
     * n'est pas trouvé dans le répertoire de travail de l'utilisateur.
     */
    @Test
    void testFindingNotFound() {
        String initialLocation = System.getProperty("user.dir");
        String fileNameToSearch = "iuerycauyroaucnr";
        List<String> matchingFiles = navigation.finding(fileNameToSearch, initialLocation);
        assertFalse(!matchingFiles.isEmpty());
    }

    /**
     * Teste l'ajout d'un favori à la liste des favoris.
     * On crée une instance de Navigation avec des favoris initiaux,
     * puis on ajoute un favori, et on vérifie que la liste des favoris
     * a été mise à jour correctement.
     */
    @Test
    void testAddFavorite() {
        List<Favorite> initialFavorites = Arrays.asList(
                new Favorite("/chemin/1", "Favori 1"),
                new Favorite("/chemin/2", "Favori 2"));

        Navigation navigation = new Navigation(initialFavorites);

        navigation.addFavorite("/chemin/3", "Favori 3");

        List<Favorite> result = navigation.getFavoriteDirectories();
        List<Favorite> expected = Arrays.asList(
                new Favorite("/chemin/1", "Favori 1"),
                new Favorite("/chemin/2", "Favori 2"),
                new Favorite("/chemin/3", "Favori 3"));

        assertEquals(expected, result);
    }

    /**
     * Teste la récupération de la liste des répertoires favoris.
     * On crée une instance de Navigation avec des favoris initiaux,
     * puis on vérifie que la méthode getFavoriteDirectories() renvoie la liste des
     * favoris.
     */
    @Test
    void testGetFavoriteDirectories() {
        List<Favorite> initialFavorites = Arrays.asList(
                new Favorite("/chemin/A", "Favori A"),
                new Favorite("/chemin/B", "Favori B"));
        Navigation navigation = new Navigation(initialFavorites);

        List<Favorite> result = navigation.getFavoriteDirectories();
        List<Favorite> expected = Arrays.asList(
                new Favorite("/chemin/A", "Favori A"),
                new Favorite("/chemin/B", "Favori B"));

        assertEquals(expected, result);
    }

    /**
     * Teste la récupération de la liste des répertoires favoris lorsque la liste
     * est vide.
     * On crée une instance de Navigation sans favoris initiaux,
     * puis on vérifie que la méthode getFavoriteDirectories() renvoie un tableau
     * vide.
     */
    @Test
    void testGetFavoriteDirectoriesEmpty() {
        List<Favorite> initialFavorites = Collections.emptyList();
        Navigation navigation = new Navigation(initialFavorites);

        List<Favorite> result = navigation.getFavoriteDirectories();
        assertEquals(Collections.emptyList(), result);
    }

}
